/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

/**
 *
 * @author JHONY QUINTERO
 */
public class MatrizBinaria implements OperacionMatriz {

    private boolean m[][];

    public MatrizBinaria() {

    }

    /*
    Metodo para cargar datos
     */
    public MatrizBinaria(String datos) throws Exception {
        if (datos.isEmpty()) {
            throw new Exception("Bobo la cadena esta vacia");
        }
        String x[] = datos.split(";");
        this.m = new boolean[x.length][];
        for (int i = 0; i < x.length; i++) {
            String z[] = x[i].split(",");
            this.m[i] = new boolean[z.length];
            for (int j = 0; j < z.length; j++) {
                if (z[j].equals("0") || z[j].equals("1")) {
                    boolean xy;
                    if (z[j].equals("1")) {
                        xy = true;
                    } else {
                        xy = false;
                    }
                    this.m[i][j] = xy;
                } else {
                    throw new Exception("Error Digito mal bobo");
                }
            }

        }
    }

    /*
    Metodo para pasar de true y false a int o decimal
     */
    public int decimal(boolean[] otro) {
        int suma = 0;
        for (int i = 0; i < otro.length; i++) {
            if (otro[otro.length - i - 1]) {
                suma += (int) Math.pow(2, i);
            }
        }
        return suma;
    }

    /*
    Retorna el vector con los numeros enteros
     */
    @Override
    public int[] getVectorDecimal() throws Exception {
        if (this.m == null) {
            throw new Exception("Error en la matriz");
        }
        int vector[] = new int[m.length];
        for (int i = 0; i < vector.length; i++) {
            vector[i] = decimal(m[i]);
        }
        return vector;
    }

    /*
    Mira si la matriz es dispersa o no
     */
    @Override
    public boolean isDispersa() throws Exception {
        if (this.m == null) {
            throw new Exception("Matriz Vacia");
        }
        for (int i = 0; i < m.length - 1; i++) {
            if (m[i].length != m[i + 1].length) {
                return true;
            }
        }
        return false;
    }

    /*
    Vector que retorna el true y false
     */
    public boolean[] getBinarioVector(String s) throws Exception {
        String vector[] = s.split(",");
        boolean var[] = new boolean[vector.length];
        for (int i = 0; i < vector.length; i++) {
            if (vector[vector.length - 1 - i].equals("true")) {
                var[i] = true;
            } else {
                var[i] = false;
            }
        }
        return var;
    }

    public String decimalABinario(int sum) throws Exception {
        String msg = "";
        int digito = 0;
        while (sum != 0) {
            digito = sum % 2;
            if (digito == 1) {
                msg += "true" + ",";
            } else {
                msg += "false" + ",";
            }
            sum /= 2;
        }
        return msg;
    }

    /*
    Sumar las dos matrices
     */
    public MatrizBinaria getSumar(MatrizBinaria m2) throws Exception {
        if (m == null || m2 == null) {
            throw new Exception("Matrices vacias");
        }
        MatrizBinaria m3 = m2;
        if (m2.m.length != this.m.length) {
            throw new Exception("no Cumplen para la suma");
        }
        for (int i = 0; i < this.m.length; i++) {
            if (this.m[i].length != m2.m[i].length) {
                throw new Exception("No cumplen la condicion");
            }
            int suma = 0;
            suma = this.decimal(this.m[i]) + this.decimal(m2.m[i]);
            m3.m[i] = this.getBinarioVector(decimalABinario(suma));
        }
        return m3;
    }

    @Override
    public String toString() {
        String msg = "";
        for (int i = 0; i < m.length; i++) {
            for (int j = 0; j < m[i].length; j++) {
                msg += this.m[i][j] + " ";
            }
            msg += "\n";
        }
        return msg;
    }
      public String toStringBinario() {
        String msg = "";
        for (int i = 0; i < m.length; i++) {
            for (int j = 0; j < m[i].length; j++) {
               if(m[i][j]==true) msg+="1"+" ";
               else msg+="0"+" ";
            }
            msg += "\n";
        }
        return msg;
    }
}

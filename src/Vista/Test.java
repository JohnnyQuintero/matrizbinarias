/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;
import Negocio.MatrizBinaria;
import Negocio.OperacionMatriz;
import javax.swing.JOptionPane;

/**
 *
 * @author JHONY QUINTERO
 */
public class Test {
    public static void main(String[] args) {
       String dato = JOptionPane.showInputDialog("Escribe la matriz");
        try {
            MatrizBinaria m = new MatrizBinaria(dato);
            System.out.println(m.toString());
            OperacionMatriz myMatriz=m;
            System.out.println("myMatriz es dispersa = "+m.isDispersa());
            int vectorDecimal[]=myMatriz.getVectorDecimal();
            for(int var:vectorDecimal)
               System.out.println(var);
            String dato2 =JOptionPane.showInputDialog("Escribe la matriz 2");
            MatrizBinaria m2 = new MatrizBinaria(dato2);
            MatrizBinaria m3=m.getSumar(m2);
            System.out.println("Mi matriz m3 es: \n"+m3.toStringBinario());
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        /*
        Matriz 1:
        true false true
        false
        false true
        
        Matriz 2:
        true false true
        false
        false true
        */

    }
}
